#!/usr/bin/python3
# coding=utf-8

# ---------------------------------------------------------
#
# Diccionarios (arrays con índices NO NUMÉRICOS!)
#

x = {'codigo': 'XF300-B2', 'ancho': 1050, 'alto': 303.25, 'hondo': 37.2}
print(x)

# Mejor así
x = {
  'codigo': 'XF300-B2',
  'ancho': 1050,
  'alto': 303.25,
  'hondo': 37.2,
}
print(x)
print()

# Acceso a elementos
print(x['codigo'])
print()

# Mezclando conceptos
print('[{:}]'.format(x['codigo']).center(40,'-'))
print('ancho.....: {:}'.format(x['ancho']))
print('alto......: {:}'.format(x['alto']))
print('hondo.....: {:}'.format(x['hondo']))
print()

# Mezclando conceptos
x = [
  {'activo': True, 'nombre': 'cpu'},
  {'activo': True, 'nombre': 'psu'},
  {'activo': False, 'nombre': 'fdd'},
  {'activo': False, 'nombre': 'gpu'},
  {'activo': True, 'nombre': 'hdd'},
]

for i in x:
  if i['activo']:
    print('[{:}] --->>> ACTIVO'.format(i['nombre'].upper()))