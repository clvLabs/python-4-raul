#!/usr/bin/python3
# coding=utf-8

# ---------------------------------------------------------
#
# Valores numéricos
#

# Hasta aquí, bien  :)
x = 123
print(x)
print()

# Decimales sin problemas
x = 123.456789
print(x)
print()

x = 10/3
print(x)
print()

# Números grandes? Lo que quieras!
x = 12345678901234567890123
print(x)
print()

x *= 12345678901234567890123
print(x)
print()

# Convertir un string a número
a = '12345'
x = int(a)
print(a)  # parece lo mismo...
print(x)  # pero con x puedes hacer operaciones aritḿeticas
print()

print(a*2)  # OSTRAS!!! (ver el ejemplo de strings)
print(x*2)
print()
