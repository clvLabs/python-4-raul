#!/usr/bin/python3
# coding=utf-8

# ---------------------------------------------------------
#
# Arrays (esto ya te suena... pero en Python son "algo distintos")
#

# Hasta aquí, bien  :)
x = [1, 2, 3, 4, 5]
print(x)
print(x[1])
print()

x = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
print(x)
print()

# Ostras !!!
x = [1, 2, 3, 'cuatro', 5]
print(x)
print()

# Y espera... que sólo empezamos!!
x = ['uno', [ 2, 'tres', 4, ], 'cinco', [[6, 7, 8], [9, 10, 11]], 'doce!' ]
print(x)

# Lo mismo... un poco más legible
x = [
  'uno',
  [ 2, 'tres', 4, ],
  'cinco',
  [
    [6, 7, 8],
    [9, 10, 11],
  ],
  'doce!'
]
print(x)
print()

# Elementos anidados
print(x[1])
print(x[1][1])
print(x[3][0][1])
print()

# Además, los arrays son modificables
x = [1, 'dos', 3]
print(x)
x[1] = 2
print(x)
x.append(4)
print(x)
x.extend([5, 6, 7])
print(x)
val = x.pop()
print('valor extraído: {:}'.format(val))
print(x)
print()

# Los índices de array en Python son un amor!!!
print(x[0])
print(x[-1])
print(x[-2])
print(x[4:])
print(x[:3])
print(x[-2:])
print(x[-1])
print()

# Y no pueden ser más fáciles de recorrer...
x = [1, 2, 3, 4, 5]
for i in x:
  print('Elemento: <{:}>'.format(i * 100))
print()

# Array comprehension (una manera MUY útil de crear arrays)
# NOTA: el parámetro end='' en print() evita el salto de línea
x = [num for num in range(100)]
for i in x:
  print('{:03d}, '.format(i), end='')
print() # Doble salto ! (uno extra por el end='' de antes)
print()

# A partir del array anterior sacamos los números pares
y = [num for num in x if num % 2 == 0]
for i in y:
  print('{:03d}, '.format(i), end='')
print()
print()
