#!/usr/bin/python3
# coding=utf-8

# ---------------------------------------------------------
#
# Valores alfanuméricos (strings)
#

# Hasta aquí, bien  :)
x = 'a'
print(x)
print()

x = 'abcdefg'
print(x)
print()

# Suma de strings
x += '*******'
print(x)
print()

# Se pueden multiplicar strings!!!
x = 'a' * 50
print(x)
print()

x = '\_/^' * 10
print(x)
print()

# ERROR !! no se puede sumar string + numero
# x = 'abcdefg' + 123
# print(x)

# ('advanced') formateo de strings
nombre = 'Les Claypool'
nacimiento = 1963
x = 'El señor {:} nació en el año {:}'.format(nombre, nacimiento)
print(x)
print()

# Los índices en Python son un amor!!!
x = 'abcdefg'
print(x[0])
print(x[-1])
print(x[-2])
print()

# Además, se pueden usar para "extraer trozos" del string
x = 'Always look at the bright side of death'
print(x[:10])
print(x[10:20])
print(x[20:-9])
print(x[-9:-4])
print(x[-4:])
print()

# Operaciones variadas con strings
x = 'algunas cosas'
y = ' y algunos espacios       '
z = ' !!!!'
print(x + y + z)
print(x + y.rstrip() + z)
print()

print('hola'.center(50, '-'))
print('hola'.capitalize())
print('hola'.upper())
print()

# Separar un string dado un string separador
x = 'cuando el grajo vuela bajo hace un frío del carajo'
print(x.split(' '))  # Aquí lo hago por espacios
print()

x = 'Pantalones,XS,15/03/2018,515,32.53'
print(x.split(','))  # En los CSV (Excel) los valores se separan con comas
print()

# Y si te pones, puedes concatenar operaciones
frase = 'Y SI te POneS, pueDES CONCATENAR operAcIOnes      '
print(frase.rstrip().lower().replace('a','4').replace('e','3'))
print()
